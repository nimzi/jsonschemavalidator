//
//  SVNull.m
//  JsonSchemaValidator
//
//  Created by Max Lunin on 5/21/13.
//  Copyright (c) 2013 Max Lunin. All rights reserved.
//

#import "SVNull.h"
#import "SVJsonSchema.h"
#import "NSString+errorMessages.h"

@implementation SVNull

+(NSString *)type
{
    return SVNullKey;
}

-(id)validateJson:(id)mustBeNull errors:(NSMutableArray *)errors
{
    if ( !mustBeNull )
        mustBeNull = self.defaultVal;
    
    id validated = [mustBeNull isKindOfClass:[NSNull class]] ? mustBeNull : nil;
    if ( !validated )
        [errors addObject:[NSString mustBeType:[self.class type] instead:[mustBeNull class]]];
    
    return validated;
}

-(NSMutableDictionary *)toJsonObject
{
    id items = [super toJsonObject];
    items[SVTypeKey] = SVNullKey;
    return items;
}

-(NSString *)description
{
    return [NSString stringWithFormat:@"%@",[self toJsonObject]];
}

@end
