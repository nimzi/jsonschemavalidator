//
//  SVArray.h
//  JsonSchemaValidator
//
//  Created by Max Lunin on 5/21/13.
//  Copyright (c) 2013 Max Lunin. All rights reserved.
//

#import "SVType.h"

@interface SVArray : SVType

@property (strong, nonatomic) id items;
@property (strong, nonatomic) NSNumber* minItems;
@property (strong, nonatomic) NSNumber* maxItems;
@property (assign, nonatomic) BOOL uniqueItems;

// nil | SVType | array of SVType's
-(instancetype)items:( id )itemsOrNill;
-(instancetype)minItems:( NSNumber* )minItems;
-(instancetype)maxItems:( NSNumber* )maxItems;
-(instancetype)uniqueItems:( BOOL )uniqueItems;

@end
