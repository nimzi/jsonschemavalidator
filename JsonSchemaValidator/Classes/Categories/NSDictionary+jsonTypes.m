//
//  NSDictionary+jsonTypes.m
//  JsonSchemaValidator
//
//  Created by Max Lunin on 23.05.13.
//  Copyright (c) 2013 Max Lunin. All rights reserved.
//

#import "NSDictionary+jsonTypes.h"
#import "SVJsonSchema.h"
#import "NSObject+jsonTypes.h"

@implementation NSDictionary (jsonTypes)

-(BOOL)isJsonObject
{
    return YES;
}

+(SVType *)jsonSchema
{
    return [SVType object];
}

-(SVType *)jsonSchema
{
    SVObject* object = (SVObject*)[self.class jsonSchema];
    
    NSMutableDictionary* schemas = [NSMutableDictionary dictionaryWithCapacity:self.count];
    [self enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL* stop)
    {
        schemas[key] = [obj jsonSchema];
    }];
    object.properties = [NSDictionary dictionaryWithDictionary:schemas];
    
    return object;
}

@end
