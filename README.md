** Simple JSON Validator for Objective-C **

More about JSON schemas you can find at [json-schema.org](http://json-schema.org)

*Install *:

```
pod 'SVJsonSchemaValidator'
```

*Using *:

Next JSON schema represent JSON with two keys:

```
#import "SVJsonSchema.h"

NSString* numberKey= @"numberKey"; // floating point nymber type in range [0, 500).
NSString* arrayKey = @"arrayKey";  //with array, containing minimun 2 strings.

SVObject* object = [[SVType object] properties:@{
                                     numberKey:[[[[SVType number]
                                                  minimum:@0] exclusiveMinimum:YES]
                                                maximum:@500],
                                      arrayKey:[[[SVType array] items:[SVType string]] minItems:@2]
                    }];

```

And simple json:

```
{ 
   "numberKey": 100500,
   "arrayKey":[ "123", "345", "678"]
}
```

Now you can validate:

```
id jsonObject = [NSJSONSerialization JSONObjectWithData:[json dataUsingEncoding:NSUTF8StringEncoding]
                                                options:0
                                                  error:NULL];

NSError* error = nil;
id validatedJson = [object validateJson:jsonObject error:&error];
```
That's all! You can check *validatedJson* variable, containing validated Objective-C objects, and *error* variable will contain error description if any errors appear.



Or you have a class
```
@interface SVModelObject : NSObject

@property (strong, nonatomic) NSString* string;
@property (strong, nonatomic) NSNumber* number;

@end
```
and JSON 
```
{
"results":[
        {"string":"1", "number":1},
        {"string":"2", "number":2},
        {"string":"3", "number":3},
        {"string":"4", "number":4},
    ]
}
```
and schema for that JSON:
```
id schema = [[SVType object] properties:@{
                     @"results":[[SVType array] items:[SVModelObject jsonSchema]]
                     }];
```

After validation you can instanciate SVModelObject's directly from JSON:

```
id validatedJson = [schema validateJson:jsonObject error:&error];
if ( !error )
{
    NSDictionary* dictionary = [schema instantiateValidatedJson:validatedJson];
}
```
After that variable *dictionary* will contain NSArray of SVModelObject by key "results".




** Types: **

SVArray - A JSON array.

SVBoolean - A JSON boolean.

SVInteger - A JSON number without a fraction or exponent part.

SVNumber -Any JSON number. Number includes integer.

SVNull - The JSON null value.

SVObject - A JSON object.

SVString - A JSON string.

SVAny - any of JSON types.

*For any of SVType you can set:*

```-defaultVal:( id ); ```// This attribute defines the default value of the instance when the instance is undefined.

```-required:( BOOL ); ```// This attribute indicates if the instance must have a value, and not be undefined. This is false by default, making the instance optional.

```-enumValues:( NSArray* ); ```// This provides an enumeration of all possible values that are valid for the instance property. Each item in the array represents a possible value for the instance value. If this attribute is defined, the instance value MUST be one of the values in the array in order for the schema to be valid.

*For SVArray: *

```-items:( id );```//This attribute defines the allowed items in an instance array, and MUST be a schema or an array of schemas. The default value is an empty schema which allows any value for items in the instance array.
When this attribute value is a schema and the instance value is an array, then all the items in the array MUST be valid according to the schema.
When this attribute value is an array of schemas and the instance value is an array, each position in the instance array MUST conform to the schema in the corresponding position for this array. This called tuple typing. 

```-minItems:( NSNumber* );```// This attribute defines the minimum number of values in an array.

```-maxItems:( NSNumber* ); ```// This attribute defines the maximum number of values in an array.

```-uniqueItems:( BOOL );```// This attribute indicates that all items in an array instance MUST be unique (contains no two identical values).


*For SVNumber or SVInteger: *

```-minimum:( NSNumber* ); ```// This attribute defines the minimum value of the instance property.

```-maximum:( NSNumber* );``` // This attribute defines the maximum value of the instance property.

```-exclusiveMinimum:( BOOL );```// This attribute indicates if the value of the instance can not equal the number defined by the "minimum" attribute. This is false by default, meaning the instance value can be greater then or equal to the minimum value.

```-exclusiveMaximum( BOOL );```// This attribute indicates if the value of the instance can not equal the number defined by the "maximum" attribute. This is false by default, meaning the instance value can be less then or equal to the maximum value.


*For SVObject: *

```-properties:( NSDictionary* );```// This attribute is an object with property definitions that define the valid values of instance object property values. The property values of the instance object MUST conform to the property definitions in this object. In this object, each property definition's value MUST be a schema, and the property's name MUST be the name of the instance property that it defines. The instance property value MUST be valid according to the schema from the property definition. Properties are considered unordered, the order of the instance properties MAY be in any order.
