//
//  SVJsonValidatorDefaultTests.m
//  JsonSchemaValidator
//
//  Created by Max Lunin on 21.05.13.
//  Copyright (c) 2013 Max Lunin. All rights reserved.
//

#import "SVJsonObjectTests.h"
#import "SVJsonSchema.h"

@implementation SVJsonObjectTests

-(void)testDefaultValue
{
    NSString* key = @"key";
    NSString* defaultValue = @"qwerty";
    SVType* scheme = [[SVType object] properties:@{ key: [[SVType string] defaultVal:defaultValue]}];
    
    NSError* error = nil;
    NSDictionary* validated = [scheme validateJson:@{} error:&error];
    STAssertNotNil(validated[key], nil);
    STAssertEqualObjects(defaultValue, validated[key], nil);
}

-(void)testPropertiesValidation
{
    id key1 = @"key1";
    id key2 = @"key2";
    SVObject* object = [[SVType object] properties:@{key1:[SVType string],
                                              key2:[SVType number]}];
    NSError* error = nil;
    STAssertNotNil( [object validateJson:@{} error:&error], nil);
    STAssertNil( error, nil);
    
    STAssertNotNil( [object validateJson:@{key1:@"qwerty"} error:&error], nil);
    STAssertNil( error, nil);
    
    STAssertNotNil( [object validateJson:@{key2:@(100500)} error:&error], nil);
    STAssertNil( error, nil);
    
    id obj = @{key1:@"qwerty", key2:@(100500) };
    STAssertNotNil( [object validateJson:obj error:&error], nil);
    STAssertNil( error, nil);
}

-(void)testWrongPropertyTypeValidation
{
    id key = @"key";
    SVObject* object = [[SVType object] properties:@{key:[SVType number]}];
    
    NSError* error = nil;
    NSDictionary* empty = [object validateJson:@{key:@"that property must be number"}
                                         error:&error];
    STAssertNil(empty, nil);
    
    NSUInteger emptyCount = 0;
    STAssertEquals(empty.count, emptyCount, nil);
    STAssertNotNil( error, nil);
}

-(void)testExtraProperties
{
    id key = @"key";
    SVObject* object = [SVType object];
    
    NSError* error = nil;
    NSDictionary* result = [object validateJson:@{key:@"string value"} error:&error];
    STAssertNotNil(result, nil);
    
    NSUInteger resultCount = 1;
    STAssertEquals(result.count, resultCount, nil);
    STAssertNil( error, nil);
}

-(void)testRequiredProperties
{
    id key = @"key";
    SVObject* requiredPropertySchema = [[SVType object] properties:@{key:[[SVType number] required:YES] }];
    
    NSError* error = nil;
    STAssertNil([requiredPropertySchema validateJson:@{}
                                                  error:&error], nil);
    STAssertNotNil( error, nil);
}

@end
