//
//  SVJsonSchemaFromInstanceTests.m
//  JsonSchemaValidator
//
//  Created by Max Lunin on 5/23/13.
//  Copyright (c) 2013 Max Lunin. All rights reserved.
//

#import "SVJsonSchemaFromInstanceTests.h"
#import "SVJsonSchema.h"

@implementation SVJsonSchemaFromInstanceTests

-(void)testString
{
    STAssertEqualObjects([[@"" jsonSchema] class], [[SVType string] class], nil);
}

-(void)testNumber
{
    STAssertEqualObjects([[@(0.5) jsonSchema] class], [[SVType number] class], nil);
}

-(void)testInteger
{
    STAssertEqualObjects([[@(0) jsonSchema] class], [[SVType integer] class], nil);
}

-(void)testNull
{
    STAssertEqualObjects([[[NSNull null] jsonSchema] class], [[SVType null] class], nil);
}

-(void)testArray
{
    STAssertEqualObjects( [[@[] jsonSchema] class], [[SVType array] class], nil);
}

-(void)testArrayWithType
{
    SVArray* schema = (SVArray*)[@[@""] jsonSchema];
    STAssertEqualObjects( [schema class], [[SVType array] class], nil);
    STAssertNotNil( schema.items, nil);
    STAssertEqualObjects( [schema.items class], [[SVType string] class], nil );
}

-(void)testArrayWithTuple
{
    SVArray* schema = (SVArray*)[@[@"1", @"2"] jsonSchema];
    STAssertNotNil(schema, nil);
    STAssertEqualObjects( [schema class], [[SVType array] class], nil);
    STAssertNotNil( schema.items, nil);
    STAssertEqualObjects( [schema.items class], [@[] class], nil);
    STAssertEqualObjects( [schema.items[0] class], [[SVType string] class], nil);
    STAssertEqualObjects( [schema.items[1] class], [[SVType string] class], nil);
}

-(void)testObject
{
    SVObject* schema = (SVObject*)[@{} jsonSchema];
    
    STAssertNotNil(schema, nil);
    STAssertEqualObjects( [schema class], [[SVType object] class], nil);
}

-(void)testObjectWithProperties
{
    id key = @"key";
    SVObject* schema = (SVObject*)[@{key:@"qwerty"} jsonSchema];
    
    STAssertNotNil(schema, nil);
    STAssertEqualObjects( [schema class], [[SVType object] class], nil);
    STAssertNotNil( schema.properties, nil);
    STAssertEqualObjects( [schema.properties class], [@{} class], nil);
    STAssertEqualObjects( [schema.properties[key] class], [[SVType string] class], nil);
}

-(void)testObjcClassForJsonObjectTypes
{
    STAssertNil( [(SVObject*)[@{} jsonSchema] objcClass] , nil);
}

-(void)testObjcClass
{
    SVObject* schema = (SVObject*)[NSObject jsonSchema];
    
    STAssertNotNil(schema, nil);
    STAssertNotNil(schema.objcClass, nil);
}

@end
