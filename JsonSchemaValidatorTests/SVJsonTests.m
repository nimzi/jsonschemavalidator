//
//  SVJsonTests.m
//  JsonSchemaValidator
//
//  Created by Max Lunin on 29.05.13.
//  Copyright (c) 2013 Max Lunin. All rights reserved.
//

#import "SVJsonTests.h"
#import "SVJsonSchema.h"

@implementation SVJsonTests

-(void)testValidate
{
    NSString* jsonString = @"{\"AuthorizedUser\":{ \"ID\":3, \"Name\":\"Тестовый пользователь\"}, \"Success55\":true }";
    NSError* error = nil;
    id json = [NSJSONSerialization JSONObjectWithData:[jsonString dataUsingEncoding:NSUTF8StringEncoding]
                                              options:0
                                                error:&error];
    STAssertNotNil(json, nil);
    STAssertNil(error, nil);
    
    id authorizationSchema = [[SVType object] properties:@{
                              @"AuthorizedUser" : [[SVType object] properties:@{
                                                   @"ID": [SVType number],
                                                   @"Name": [SVType string]
                                                   }],
                              @"Success": [SVType boolean]
                              }];

    NSDictionary* validated = [authorizationSchema validateJson:json error:&error];
    STAssertNotNil(validated, nil);
    STAssertNil(error, nil);
    NSUInteger keysCount = 2;
    STAssertEquals(validated.count, keysCount, nil);
}

-(void)testValidateRequred
{
    NSString* jsonString = @"{\"AuthorizedUser\":{ \"ID\":3, \"Name\":\"Тестовый пользователь\"}, \"Success55\":true }";
    NSError* error = nil;
    id json = [NSJSONSerialization JSONObjectWithData:[jsonString dataUsingEncoding:NSUTF8StringEncoding]
                                              options:0
                                                error:&error];
    STAssertNotNil(json, nil);
    STAssertNil(error, nil);
    
    id authorizationSchema = [[SVType object] properties:@{
                              @"AuthorizedUser" : [[SVType object] properties:@{
                                                   @"ID": [[SVType number] required:YES],
                                                   @"Name": [[SVType string] required:YES]
                                                   }],
                              @"Success": [[SVType boolean] required:YES]
                              }];
    
    NSDictionary* validated = [authorizationSchema validateJson:json error:&error];
    STAssertNil(validated, nil);
    STAssertNotNil(error, nil);
}


@end
