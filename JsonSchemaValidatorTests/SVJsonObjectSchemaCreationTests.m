//
//  SVJsonObjectSchemaCreationTests.m
//  JsonSchemaValidator
//
//  Created by Max Lunin on 5/23/13.
//  Copyright (c) 2013 Max Lunin. All rights reserved.
//

#import "SVJsonObjectSchemaCreationTests.h"
#import "SVJsonSchema.h"

@implementation SVJsonObjectSchemaCreationTests

-(void)testPropertiesNull
{
    SVObject* object = [SVObject schemaWithDictionary:@{SVTypeKey: SVObjectKey, SVPropertiesKey:[NSNull null]}];
    STAssertNotNil(object, nil);
    STAssertNil( object.properties, nil);
}

-(void)testPropertiesArray
{
    SVObject* object = [SVObject schemaWithDictionary:@{SVTypeKey: SVObjectKey, SVPropertiesKey:@[]}];
    STAssertNotNil(object, nil);
    STAssertNil( object.properties, nil);
}

-(void)testPropertiesEmpty
{
    SVObject* object = [SVObject schemaWithDictionary:@{SVTypeKey: SVObjectKey, SVPropertiesKey:@{}}];
    STAssertNotNil(object, nil);
    STAssertNotNil( object.properties, nil);
}

-(void)testProperties
{
    id key = @"qwerty";
    SVObject* object = [SVObject schemaWithDictionary:@{SVTypeKey: SVObjectKey, SVPropertiesKey:@{key: @{SVTypeKey: SVStringKey}}}];
    STAssertNotNil(object, nil);
    STAssertNotNil( object.properties, nil);
    STAssertNotNil( object.properties[key], nil);
    STAssertEqualObjects([object.properties[key] class], [SVString class], nil);
}

-(void)testObjcClass
{
    SVObject* object = [SVObject schemaWithDictionary:@{SVTypeKey: SVObjectKey, SVObjcClassKey: NSStringFromClass([NSObject class])}];
    STAssertNotNil(object, nil);
    STAssertNotNil(object.objcClass, nil);
    STAssertEqualObjects(object.objcClass, [NSObject class], nil);
}

@end
